

# Development Guidelines

## Environment

### Files

#### File Names
Since we also install our applications on an SAP NetWeaver Application Server, there are special rules for naming files and folders.

* Folder and file names must not contain spaces
* Folder and file names must not contain non-ASCII characters
* Folder and file names must not exceed 40 characters in length
* Folder and file names must not only differ in case

#### Encoding
All files should be UTF-8 encoded. The end of a line (EOL) should be ended with a single line feed (LF, unix-style).

## Source Code

### Linting
We used ESLint to statically analyze the source code. The source code must be free of ESLint errors before it can be committed or pushed, otherwise the CI/CD pipeline will not run.

### Formatting
The source code should be formatted at all times. There are different rules for the different file formats.

#### JavaScript/JSON
JavaScript and JSON files should be formatted with the built-in Beautifier (Pretty Printer). This can be called as follows:

| Editor | Shortcut |
|--------|----------|
| VSCode | <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>F</kbd> |
| SAP WebIDE | <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>B</kbd> |


#### XML
For XML files, the currently existing Beautifiers are insufficient, since they can only indent individual tags as a whole. This can become ugly if an XML tag has a lot of attributes, because then the entire XML tag is formatted into a very long line.

In editors that do not have a suitable plugin, we have to format the source code manually according to the following rules:

* Each attribute is placed in a separate line
* Complex DataBinding expressions are wraped meaningfully
* The order of the attributes is as follows
  * ID
  * Properties (e.g. `value`, `maxLength`, `showHeader`)
  * Aggregations (e.g. `items`, `rows`, `content`)
  * Events (e.g. `press`, `select`, `uploadFinished`)
* Empty elements, i.e. elements without child elements, are declared without an end tag, but must be marked as empty by a slash at the end.
  * <b style="color: limegreen">Good:</b> `<Button />`
  * <b style="color: crimson">Bad:</b> `<Button></Button>`

For example:

```xml
<Input id="categoryInput"
    description="Test"
    maxLength="{
        path: '/#Category/CategoryName/@maxLength',
        formatter: 'parseInt'
    }"
    type="Password"
    value="{Category}" />
```
___

> Josh Johnson's VSCode plugin **XMLTools** can format XML to meet most of our requirements starting with version 2.3.0.

Open the user settings via `File > Settings > Settings` and adjust the following values:

```json  
"xmlTools.enforcePrettySelfClosingTagOnFormat": true,
"xmlTools.splitAttributesOnFormat": true
```

You can then format XML files similar to JavaScript files using the shortcut <kbd>Alt</kbd> + <kbd>Shift</kbd> + <kbd>F</kbd>.

### File Structure

#### JavaScript

The source code should be well structured at all times. To ensure that this is the case, we recommend that you arrange the methods in the following order:

1. Delegates (i.e. outsourced functionalities, such as a formatter file)
1. Constructor (if available)
1. Lifecycle hooks (`onInit`, `onBeforeRendering`, etc.)
1. Event handler (for events called by controls in the corresponding view)
1. Private methods (other events, helper methods, etc.)

For example:

```javascript
sap.ui.define([
    "./BaseController",
    "../util/Formatter"
], function (BaseController, Formatter) {

    "use strict";
    return BaseController.extend("de.template.controller.Main", {

        /**
         * Delegates
         */

        formatter: Formatter,
        
        /**
         * Lifecycle hooks
         */

        onInit: function () {
            this._doThings();
        },
        
        /**
         * Event handler
         */
            
        onPressButton: function (oEvent) {
            this._doStuff();
        },
        
        /**
         * Private methods
         */

        _doThings: function() { },

        _doStuff: function() { }
    });
});
```

### Variable Declaration
> This can be automatically fixed by ESLint (`no-var`, `prefer-const`).

Variables should never be declared without a keyword, otherwise they will be included in the global scope (that is, they will be available as global variables throughout the whole application, which may be the entire Fiori Lauchpad).

We do not use `var`. Instead, we declare and initialize variables only with `let` and `const`.

### Variable Names
> This can be flagged but not automatically fixed by ESLint (`id-match`).

When naming variables, we follow SAP's official guidelines for their UI5 development. The various variable types, including an example, are listed below:

| Type | Example |
|------|---------|
| Boolean | `const bVisible = true;`
| Integer | `const iCount = 0;`
| Float | `const fWeight = 24.77;`
| Regular Expression | `const rPattern = /^.*@.*\..*/;`
| String | `const sName = "Test";`
| Date | `const dStartDate = new Date();`
| Array | `const aItems = [1, 3, 5, 7];`
| Object | `const oButton = new sap.m.Button();`
| Function | `const fnDoNothing = function() {};`
| Variant | `let vValue = "5";`<br>`vValue = parseInt(vValue) * 2;` |


### Method Names
The method names should of course be clear, concise and consistent. A method named `check` does not reveal exactly what it does.
Method names should start with a verb and then become more specific. Nevertheless, method names should not be excessively long.

* <b style="color: crimson">Bad:</b> `check`
* <b style="color: crimson">Less bad:</b> `checkEmptinessOfContainer`
* <b style="color: limegreen">Good:</b> `isContainerEmpty`

>Event handlers are an exception. These should always begin with on, followed by the event name and a description of the control that triggers this event.

The general naming scheme of an event handler is as follows:

`on[EVENT_NAME][CONTROL_DESCR[CONTROL_NAME?]]`

For example:

* `onPressAppointment`
* `onPressRefreshButton`
* `onChangeCountry` / `onChangeCountrySelect`
* `onCompleteUserSelectionStep`

### Method/Attribute Visibility
There is no built-in concept for the visibility of methods and class attributes in JavaScript. Such a concept can therefore only be implemented through conventions. A method/attribute is private by convention if an underscore is placed before the actual name, e.g. `_onRouteMatched`. In addition, all private methods should be sorted in the lower area of a JavaScript file.

Basically, all methods and attributes that are not used in other files must be marked as private.

### Model Names

#### Local Models
The model for translatable text is always called `i18n`.  
The model in which information about the current view is held is always called `view`.

All other local models should have a meaningful name, but without the word "model" at the end. The spelling is lowerCamelCase.

Some examples:

* <b style="color: crimson">Bad:</b> `deviceModel`
* <b style="color: crimson">Bad:</b> `Device`
* <b style="color: limegreen">Good:</b> `device`


#### OData Models
If only one OData service is used in the app, the model does not get a name (i.e. an empty string in the `manifest.json`).
If not, a descriptive name must be chosen here as well. To distinguish it from the local models, the suffix `Remote `is appended. The spelling is lowerCamelCase. 

Some examples:

* <b style="color: crimson">Bad:</b> `talentPoolRemoteModel`
* <b style="color: crimson">Bad:</b> `TalentPoolModel`
* <b style="color: crimson">Bad:</b> `TalentPool`
* <b style="color: limegreen">Good:</b> `talentPoolRemote`

#### Usage of Models in JavaScript files
Often a model is accessed not only in a view, but also in the corresponding controller. To avoid flooding the controller with string literals, we use constants for the model names, which can then be used in further source code.

For example:
```javascript
sap.ui.define([
    "../BaseController"
], function (BaseController) {
    "use strict";

    const USER_MODEL = "user";
    const VIEW_MODEL = "view";
    const TALENTREMOTE_MODEL = "talentRemote";

    return BaseController.extend("de.test.controller.Talent", {
        
        onInit: function() {
            const oModel = this.getModel(_TALENTREMOTEMODEL);
            ...
        },

        ...
    });
});
```

### Property Names
Here we stick to the OData standard: We write the names of properties in *UpperCamelCase*. Of course the names are clear concise and consistent.

### Internationalization

#### Text
All texts that are visible in the app (labels, buttons, dialogs, message toasts, list and table titles, etc.) should be translatable.

If a text is loaded from the back end, it should be translated there.

Text defined in the UI5 app should be stored in an `i18n.properties` file. In individual views, fragments and controllers, there should be no display texts that are hard-coded.

#### Date/Number
Date and number values, which also include currency amounts, must be displayed at the frontend according to the respective country setting.

The data types built into UI5 are to be used for this purpose. You can then specify generic formatting options for the data type, which are then converted correctly in each country setting.

For example:
```xml
<Text text="{
        path: 'taskRemote>CreateTimestamp', 
        type: 'sap.ui.model.type.DateTime', 
        formatOptions: { style: 'short' } 
    }" />
```
Leads to the following output (when the locale is set to `de-DE`):

>30.01.18, 13:49:01

If the browser is set to `en-US`, the output looks like this:

>1/30/18, 1:49 PM

### Styles
Custom CSS should not be used due to the difficult maintainability. If it is necessary to use custom CSS (for example, for custom controls or display problems with standard controls), you must observe the following guidelines:

1. **Use your own CSS classes.**  
So that a CSS rule does not affect unwanted controls, a new CSS class should be assigned to the corresponding controls that we want to customize with the Property class.
1. **Prefix your custom CSS class.**  
To distinguish our CSS classes from the standard classes, we prefix them.
1. **Semantically name CSS classes.**  
The name of the class should not indicate technical details, since these are completely irrelevant at first glance.
Instead of specifying the effect, the class name should describe the effect, e.g. `myErrorText` instead of `myRedText`.
1. <b style="color: crimson">**Never ever use `important!`**</b>


## Dos and Dont's
This section covers common worst and best practices

### Defining/Requiring Modules
We use the recommended AMD-like module syntax.

<b style="color: limegreen">**Do: Define modules asynchronously**</b>

```javascript
sap.ui.define([
    "sap/m/Button",
    "sap/m/MessageBox"
], function(Button, MessageBox) {
    "use strict";

    return Button.extend("my.Button", {
        ...
        onclick: function(oEvent) {
            MessageBox.show("test");
        }
    })
});
```

<b style="color: limegreen">**Do: Lazy require modules asynchronously**</b>

```javascript
sap.ui.define([
    "sap/m/Button"
], function(Button) {
    "use strict";

    return Button.extend("my.Button", {
        ...
        onclick: function(oEvent) {

            // asynchronous lazy require
            sap.ui.require([
                "sap/m/MessageBox"
            ], function(MessageBox) {
                MessageBox.show("test");
            });
        }
    })
});
```

<b style="color: crimson">**Don't: Define/require modules synchronously**</b>

The methods `jQuery.sap.declare` and `jQuery.sap.require` load and evaluate modules synchronously. This is classified as *deprecated* by both the UI5 framework and modern browsers.

Accessing controls via their fully qualified module name also loads them snychronously, which is deprecated and also hurts performance.

```javascript
jQuery.sap.declare("my.Button");
jQuery.sap.require("sap.m.Button");

var Button = sap.m.Button.extend("my.Button", {
    ...
    onclick: function(oEvent) {
        // synchronous lazy require
        sap.m.MessageBox.show("test");
    }
});
```

### Controller Inheritance
<b style="color: limegreen">**Do: Inherit from local BaseController and use helper methods**</b>

```javascript
sap.ui.define([
    "./BaseController"
], function (BaseController) {
    "use strict";

    return BaseController.extend("de.test.template.Main", {
        ...
        onAfterRendering: function() {
            const oModel = this.getModel();
        }
```

<b style="color: crimson">**Don't: Inherit from sap.ui.core.mvc.Controller**</b>

If not inherited from the `BaseController`, either helper methods must be defined several times in the individual controllers or unnecessary method chaining is used in many places.
```javascript
sap.ui.define([
    "sap/ui/core/mvc/Controller"
], function (Controller) {
    "use strict";

    return Controller.extend("de.test.template.Main", {
        ...
        onAfterRendering: function() {
            const oModel = this.getView().getModel();
        }
```

### Coupling of View and Controller
<b style="color: limegreen">**Do: Use a view model**</b>

```xml
<Input value="{view>/Name}" />
```

```javascript
const oViewModel = this.getModel("view");
const sName = oViewModel.getProperty("/Name");
```

<b style="color: crimson">**Don't: Access controls directly**</b>

```xml
<Input id="nameInput" />
```

```javascript
const oControl = this.byId("nameInput");
const sName = oControl.getValue();
```

### Usage of `this` in callback functions
> This can be flagged but not automatically fixed by ESLint (`consistent-this`).

<b style="color: limegreen">**Do: Use `that`**</b>

>By convention, we make a private that variable. This is used to make the object available to the private methods. This is a workaround for an error in the ECMAScript Language Specification which causes this to be set incorrectly for inner functions. - *Douglas Crockford*

```javascript
const that = this;
const oModel = this.getModel();

oModel.metadataLoaded().then(function () {
    const sKey = oModel.createKey("/OrderSet", {
        Auftrid: sAuftragsid
    });
    that.getView().bindElement(sKey);
});
```

<b style="color: limegreen">**Do: Use arrow functions`**</b>

>Arrow functions use lexical scoping. `this` refers to its current surrounding scope and no further.

```javascript
const oModel = this.getModel();

oModel.metadataLoaded().then(() => {
    const sKey = oModel.createKey("/OrderSet", {
        Auftrid: sAuftragsid
    });
    this.getView().bindElement(sKey);
});
```

<b style="color: crimson">**Don't: Bind callback function to `this`**</b>

Binding a function to a specific `this` value is significantly slower than creating a closure and hurts performance.

```javascript
var oModel = this.getModel();

oModel.metadataLoaded().then(function () {
    var sKey = oModel.createKey("/OrderSet", {
        Auftrid: sAuftragsid
    });
    this.getView().bindElement(sKey);
}.bind(this);
```

### OData Request Path
<b style="color: limegreen">**Do: Use UI5 API to create entity key**</b>

```javascript
const oModel = this.getModel();
const that = this;
oModel.metadataLoaded().then(function () {
    const sKey = oModel.createKey("/OrderSet", {
        Auftrid: sAuftragsid
    });
    that.getView().bindElement(sKey);
});
```

<b style="color: crimson">**Don't: Build the entity key manually**</b>

```javascript
const oModel = this.getModel();
const sKey = "/OrderSet(AuftragsId=" + sAuftragsid + ")";
this.getView().bindElement(sKey);
```

### Comparisons
<b style="color: crimson">**Don't: Use logical operators incorrectly**</b>

Contrary to popular belief, a statement which uses logical operators does not necessarily return a boolean value.

```javascript
let sString = "test";
console.log(sString && sString.length > 0); // true
console.log(sString.length > 0 && sString); // "test"

sString = "";
console.log(sString && sString.length > 0); // "" (empty string)
console.log(sString.length > 0 && sString); // false
```

If a statement containing the `&&` operator (AND) is truthy the right operand is returned, if it is falsy the left operand is returned.

With the `||` operator (OR) it is the other way round: If the statement is truthy the left operand is returned, if it is falsy the right operand is returned.

### Framework APIs

<b style="color: limegreen">**Do: Use public APIs**</b>

Public methods of UI5 classes are tested and documented. If a method becomes deprecated it will be announced well in advance.

```javascript
const oWizard = this.byId("wizard");
oWizard.nextStep();
```

<b style="color: crimson">**Don't: Use private APIs**</b>

Private methods of any UI5 class can be modified or even deleted at any time. Just because the code works with our current version doesn't guarantee that it will also work  when the next patch or update is released.

```javascript
const oWizard = this.byId("wizard");
oWizard._setNextStep("step3");
```
